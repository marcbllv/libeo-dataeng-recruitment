from typing import Any, Dict, List

import pandas as pd
from sqlalchemy.engine import Engine, create_engine

db_host = "34.78.217.101"
db_user = "postgres"
db_password = "oeGAsdlkHFKeFMMa"
db_name = "libeo"
warehouse_schema = "warehouse"


def _get_engine() -> Engine:
    conn_string = f"postgresql+psycopg2://{db_user}:{db_password}@{db_host}/{db_name}"
    connect_args = {"options": f"-csearch_path={warehouse_schema}"}
    return create_engine(conn_string, connect_args=connect_args)


def push_to_warehouse(
    table_name: str,
    rows: List[Dict[str, Any]],
    if_exists: str = "replace",
):
    results = pd.DataFrame(rows)
    engine = _get_engine()
    with engine.connect() as conn:
        results.to_sql(
            table_name,
            conn,
            if_exists=if_exists,
            chunksize=3000,
            index=False,
        )
