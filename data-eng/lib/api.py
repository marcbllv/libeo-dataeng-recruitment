import re
from typing import Optional

from sqlalchemy.engine import Engine, create_engine

db_host = "34.78.217.101"
db_user = "postgres"
db_password = "oeGAsdlkHFKeFMMa"
db_name = "libeo"
product_db_schema = "product_db"


def _get_engine() -> Engine:
    conn_string = f"postgresql+psycopg2://{db_user}:{db_password}@{db_host}/{db_name}"
    return create_engine(conn_string)


def api_call(
    url: str,
    updated_after: Optional[str] = None,
    limit: int = 10,
    sort_by: Optional[str] = None,
    offset: Optional[int] = 0,
):
    limit = min([limit, 100])

    url_matches = re.match(r"https://api.libeo.io/v1/(\w+)/(\w+)", url)
    if url_matches is None:
        raise ValueError(
            f"Invalid URL {url} - "
            f"Expected https://api.libeo.io/v1/{{object}}/{{action}}"
        )
    entity = url_matches.group(1)
    url_matches.group(2)

    valid_entities = ("organisation", "invoice", "user")
    if entity not in valid_entities:
        raise ValueError(f"Unknown entity {entity}, must be one of {valid_entities}")

    sql_query_parts = [f'SELECT * FROM {product_db_schema}."{entity}"']
    if updated_after is not None:
        sql_query_parts.append(f"WHERE updated_at >= '{updated_after}'")
    if sort_by is not None:
        sql_query_parts.append(f"ORDER BY {sort_by}")
    sql_query_parts.append(f"LIMIT {limit}")
    if offset is not None:
        sql_query_parts.append(f"OFFSET {offset}")
    sql_query = " ".join(sql_query_parts)

    engine = _get_engine()
    with engine.connect() as conn:
        results = conn.execute(sql_query)

    column_names = results.keys()
    return {
        "results": [
            {column: result_value for column, result_value in zip(column_names, result)}
            for result in results
        ]
    }
