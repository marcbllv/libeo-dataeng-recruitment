from datetime import datetime

import click
from etl.app_db_etl import run_production_db_etl
from sqlalchemy.engine import Engine, create_engine

db_host = "34.78.217.101"
db_user = "postgres"
db_password = "oeGAsdlkHFKeFMMa"
db_name = "libeo"

source_schema = "public"
product_db_schema = "product_db"
warehouse_schema = "warehouse"
tables = ["user", "organisation"]


def _get_engine(schema: str) -> Engine:
    conn_string = f"postgresql+psycopg2://{db_user}:{db_password}@{db_host}/{db_name}"
    connect_args = {"options": f"-csearch_path={schema}"}
    return create_engine(conn_string, connect_args=connect_args)


def check_all_table_count_match():
    engine = _get_engine(schema="product_db")
    print("    Checking prod & warehouse tables have same count...", end=" ")
    for table in tables:
        check_same_count_query = f"""
            WITH warehouse_count AS (
              SELECT count(*) AS c FROM warehouse."{table}"
            ),

            prod_count AS (
              SELECT count(*) AS c FROM product_db."{table}"
            )

            SELECT warehouse_count.c = prod_count.c AS count_match
            FROM warehouse_count
            CROSS JOIN prod_count
        """
        with engine.connect() as conn:
            results = conn.execute(check_same_count_query)
        for result in results:
            if not result["count_match"]:
                print("❌ Fail")
                raise RuntimeError(f"Row count mismatch for table {table}")
    print("✅ OK")


def check_all_table_content_match():
    engine = _get_engine(schema="product_db")
    print("    Checking prod & warehouse tables have same content...", end=" ")
    for table in tables:
        check_same_count_query = f"""
            SELECT count(*) = 0 AS has_missing_rows
            FROM warehouse."{table}" warehouse
            FULL OUTER JOIN product_db."{table}" prod
              USING (id)
            WHERE warehouse.id IS NULL OR prod.id IS NULL
        """
        with engine.connect() as conn:
            results = conn.execute(check_same_count_query)
        for result in results:
            if not result["has_missing_rows"]:
                print("❌ Fail")
                raise RuntimeError(f"Missing rows in table {table}")
    print("✅ OK")


def reset_prod_and_warehouse():
    # Reset DB and populate for first run
    product_db_engine = _get_engine(schema="product_db")
    for table in tables:
        drop_table_wh_query = f'DROP TABLE IF EXISTS {warehouse_schema}."{table}"'
        drop_table_prod_query = f'DROP TABLE IF EXISTS {product_db_schema}."{table}"'

        with product_db_engine.connect() as conn:
            conn.execute(drop_table_wh_query)
            conn.execute(drop_table_prod_query)


def populate_for_run(start_date: str, end_date: str, create_table: bool = False):
    product_db_engine = _get_engine(schema="product_db")
    for table in tables:
        if create_table:
            insert_or_create = f'CREATE TABLE {product_db_schema}."{table}" AS'
        else:
            insert_or_create = f'INSERT INTO {product_db_schema}."{table}"'
        select_query = f"""
              SELECT * FROM {source_schema}."{table}"
              WHERE updated_at < '{end_date}'
                AND updated_at >= '{start_date}'
        """
        populate_table_query = " ".join([insert_or_create, select_query])
        with product_db_engine.connect() as conn:
            conn.execute(populate_table_query)


def first_run():
    populate_for_run(start_date="2020-10-01", end_date="2021-02-01", create_table=True)

    print("[2021-02-01 00:00:00] Running ETL to retrieve product DB")
    run_production_db_etl(
        state={"execution_timestamp": datetime(2021, 2, 1), "is_first_sync": True},
    )
    print()
    check_all_table_count_match()
    check_all_table_content_match()
    print()


def second_run():
    populate_for_run(start_date="2021-02-01", end_date="2021-02-02")

    print("[2021-02-02 00:00:00] Running ETL to retrieve product DB")
    run_production_db_etl(
        state={"execution_timestamp": datetime(2021, 2, 2), "is_first_sync": False},
    )
    print()
    check_all_table_count_match()
    check_all_table_content_match()
    print()


def third_run():
    populate_for_run(start_date="2021-02-02", end_date="2021-02-03")

    print("[2021-02-03 00:00:00] Running ETL to retrieve product DB")
    run_production_db_etl(
        state={"execution_timestamp": datetime(2021, 2, 3), "is_first_sync": False},
    )
    print()
    check_all_table_count_match()
    check_all_table_content_match()
    print()


@click.command()
@click.option("--run-date", default="all")
def main(run_date: str):
    if run_date == "all":
        reset_prod_and_warehouse()
        first_run()
        second_run()
        third_run()
    elif run_date == "2021-02-01":
        reset_prod_and_warehouse()
        first_run()
    elif run_date == "2021-02-02":
        second_run()
    elif run_date == "2021-02-03":
        third_run()
    else:
        print(f"Invalid date '{run_date}'")


if __name__ == "__main__":
    main()
