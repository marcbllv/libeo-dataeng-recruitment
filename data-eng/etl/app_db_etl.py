"""
This module contains an ETL that pulls production DB data and load it to our warehouse.

Function `run_production_db_etl` is called every night at midnight.
State parameter is a dictionary that contains information regarding the pull context.
Especially `state["execution_timestamp"]` contains the current timestamp of a given
call to the function and `state["is_first_sync"]` contains True/False whether this
is the first time we call the ETL.
"""
from lib.api import api_call
from lib.push_to_warehouse import push_to_warehouse


def run_production_db_etl(state):
    # orga
    o = 0
    orgas = []
    print("Pulling organisations table")
    while True:
        r = api_call("https://api.libeo.io/v1/organisation/list", limit=100, offset=o)
        if len(r["results"]) > 0:
            orgas.extend(r["results"])
        else:
            break
        o += 100

    print("Pushing organisations to warehouse")
    push_to_warehouse("organisation", orgas)

    # user
    o = 0
    users = []
    print("Pulling users table")
    while True:
        u = api_call("https://api.libeo.io/v1/user/list", limit=100, offset=o)
        if len(u["results"]) > 0:
            users.extend(u["results"])
        else:
            break
        o += 100

    print("Pushing users to warehouse")
    push_to_warehouse("user", users)
