from lib.api import api_call


def test_api_call_works():
    results = api_call("https://api.libeo.io/v1/user/list", "2021-01-01", 10)
    assert len(results["results"]) == 10
